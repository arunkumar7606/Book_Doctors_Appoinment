package com.arun.aashu.doctorappoinment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {


    AlertDialog.Builder ab;
    String bc;
    Button plus, minus;
    Switch switchBtn;
    Spinner spinner;
    String r;
    TextView textvale;
    RadioGroup radioGroup;
    RadioButton r1,r2;
    int count,y=0;

    ArrayList<String> arList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = findViewById(R.id.lastName);
        plus = findViewById(R.id.plus);
        minus = findViewById(R.id.minus);
        textvale = findViewById(R.id.textvalue);
        radioGroup=findViewById(R.id.rdgroup);
        r1=findViewById(R.id.radio1);
        r2=findViewById(R.id.radio2);
        switchBtn =findViewById(R.id.switchButton);

        textvale.setText("0");
        minus.setEnabled(false);




        String data[] = getResources().getStringArray(R.array.data);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data);
        spinner.setAdapter(adapter);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {


                if (r1.isChecked()){
                    r=r1.getText().toString();
                }
                else if (r2.isChecked()){
                    r=r2.getText().toString();
                }else{
                    r=r2.getText().toString();
                }

            }
        });


        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (y!=3){

                    y++;
                    minus.setEnabled(true);
                }
                else if (y==3){

                    plus.setEnabled(false);
                    minus.setEnabled(true);
                    Toast.makeText(MainActivity.this, "Maximum Persons are only 3 allowed", Toast.LENGTH_SHORT).show();

                }
                textvale.setText(String.valueOf(y));

            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (y!=0) {

                    plus.setEnabled(true);
                    y--;
                }
                else if(y==0){

                    minus.setEnabled(false);
                    plus.setEnabled(true);

                }
                textvale.setText(String.valueOf(y));
            }
        });



        ((EditText) findViewById(R.id.weight)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar ca = Calendar.getInstance();

                new TimePickerDialog(MainActivity.this, time,
                        ca.get(Calendar.HOUR_OF_DAY),
                        ca.get(Calendar.MINUTE), true).show();


            }
        });


        findViewById(R.id.height).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar ca = Calendar.getInstance();
                new DatePickerDialog(MainActivity.this, dateListener,
                        ca.get(Calendar.YEAR), ca.get(Calendar.MONTH),
                        ca.get(Calendar.DAY_OF_MONTH)).show();

            }
        });


        ((Switch) findViewById(R.id.switchButton)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


                if (switchBtn.isChecked()) {

                    bc = "Married";
                }
                else {
                    bc = "Unmarried";

                }
            }
        });

        findViewById(R.id.register_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ab = new AlertDialog.Builder(MainActivity.this);
                ab.setTitle("Book Doctor's Appoinment");
                ab.setMessage("Have you filled the Requied Details for Booking then Submit");
                ab.setIcon(R.mipmap.ic_launcher_round);

                ab.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        count++;
                        String token= String.valueOf(count);

                        arList.add("Your Appoinment number is: QRCF76"+token+
                                "\n Name: " + ((EditText) findViewById(R.id.email)).getText().toString()
                                + "\n Gender: " + ((TextView) findViewById(R.id.password)).getText().toString()
                                +r
                                + "\n Contact_No: " + ((EditText) findViewById(R.id.firstName)).getText().toString()
                                + "\n doctor type: " + spinner.getSelectedItem().toString()
                                + "\n Number Of Persons: " + ((TextView) findViewById(R.id.textvalue)).getText().toString()
                                +textvale.getText().toString()
                                + "\n day: " + ((EditText) findViewById(R.id.height)).getText().toString()
                                + "\n Time: " + ((EditText) findViewById(R.id.weight)).getText().toString()
                                + "\n Maritials Status: " + bc);



                        Intent intent = new Intent(MainActivity.this, SeondActivity.class);
                        intent.putExtra("list", arList);
                        startActivityForResult(intent, 0);

                        ((EditText) findViewById(R.id.email)).setText("");
                        ((TextView) findViewById(R.id.password)).setText("");
                        ((EditText) findViewById(R.id.firstName)).setText("");
                        textvale.setText("0");
                        ((EditText) findViewById(R.id.height)).setText("");
                        ((EditText) findViewById(R.id.weight)).setText("");

                    }
                });


                ab.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                });


                ab.show();
            }
        });


    }


    TimePickerDialog.OnTimeSetListener time = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hourOfday, int minute) {

            ((EditText) findViewById(R.id.weight)).setText(hourOfday + " : " + minute);


        }
    };

    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int Year, int month, int dayofMonth) {

            ((EditText) findViewById(R.id.height)).setText(dayofMonth + "/" + month + "/" + Year);


        }
    };

    @Override
    protected void onActivityResult(int requestcode, int resultCode, Intent data1) {

        if (requestcode == 0 && resultCode == 420 && data1 != null) {
            int p = data1.getExtras().getInt("position");

            arList.remove(p);
        }
    }


}
